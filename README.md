# README #

This repo contains code and notes from studying [Master NestJS - The JavaScript Node.js Framework](https://www.udemy.com/course/master-nestjs-the-javascript-nodejs-framework/).

## Project ##

The course uses the NestJS CLI.
Install the CLI on your system using `npm install -g @nestjs/cli`.
Verify it is installed with `nest --help`.
Crete new project with `nest new nest-events-backend`.

To run the project:

```bash
cd nest-events-backend
npm run start:dev
```

## Links ##

[NestJS with monorepo](https://docs.nestjs.com/cli/monorepo)

## Notes ##

Convention for controller method names.

Keep short names. Make the resources thin. Controller should not have more than 5 actions:

```js
import { Body, Controller, Delete, Get, Param, Patch, Post } from "@nestjs/common";

@Controller('/events')
export class EventsController {
  @Get()
  findAll() { }
  @Get(':id')
  findOne(@Param('id') id) { return id; }
  @Post()
  create(@Body() input) { return input; }
  @Patch(':id')
  update(@Param('id') id, @Body() input) { return input; }
  @Delete(':id')
  remove(@Param('id') id) { }
}
```

Getting parameters:

- `@Param(':id') id` gets one parameter
- `@Param() parameters` gets object with all parameters

